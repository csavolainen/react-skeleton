//= require jquery
//= require jquery_ujs
//= require foundation
//= require react
//= require signals
//= require crossroads
//= require native.history
//= require lodash
//= require namespaces
//= require_tree .

$(function(){ $(document).foundation(); });
