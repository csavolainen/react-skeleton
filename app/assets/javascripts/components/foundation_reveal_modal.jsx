/**
 * @jsx React.DOM
 */
(function(React, components) {
    "use strict";

    components.foundation.RevealModal = React.createClass({
        handleComplete: function() {
           $('#' + this.props.id).foundation('reveal', 'close');
        },

        render: function () {
            return (
                <div id={this.props.id} className='reveal-modal large' data-reveal>
                    {this.props.children}

                    <a className="close-reveal-modal">&#215;</a>
                </div>
            );
        },

        setupOnComplete: function () {
            if( Array.isArray(this.props.children) ) {
                this.props.children.each(this.setupChildOnComplete);
            } else {
                this.setupChildOnComplete(this.props.children);
            }
        },

        setupChildOnComplete: function(child) {
            this.props.children.props.onComplete = this.handleComplete;
        }
    });

})(window.React, window.app.components);
