/**
 * @jsx React.DOM
 */
(function (React, components) {
    "use strict";

    components.GoogleMap = React.createClass({
        getInitialState: function () {
            return {markers: [], infoWindow: null};
        },

        componentDidMount: function (rootNode) {
            var mapOptions = {
                center: this.mapCenterLatLng(),
                zoom: 8
            },
            map = new google.maps.Map(rootNode, mapOptions),
            markers = this.props.markers.map(this.makeMarker),
            infoWindow = this.makeInfoWindow(this.props.infoWindow);

            this.setState({map: map, markers: markers, infoWindow: infoWindow});
        },

        makeMarker: function (rinkMarker) {
            var markerOptions = {
                position: new google.maps.LatLng(rinkMarker.lat, rinkMarker.lng),
                animation: google.maps.Animation.DROP
            };

            return new google.maps.Marker(markerOptions);
        },

        makeInfoWindow: function (options) {
            if( options ) {
                var infoWindowOptions = {
                    position: new google.maps.LatLng(options.lat, options.lng),
                    content: options.content
                };

                return new google.maps.InfoWindow(infoWindowOptions);
            }
        },

        mapCenterLatLng: function () {
            var props = this.props;

            return new google.maps.LatLng(props.mapCenterLat, props.mapCenterLng);
        },

        componentWillReceiveProps: function (newProps) {
            var markersToRemove = _.reject(this.state.markers, function (marker) {
                var position = marker.getPosition(),
                predicate = {lat: position.lat(), lng: position.lng()};
                return _.detect(newProps.markers, predicate);
            });

            var remainingMarkers = _.difference(this.state.markers, markersToRemove);
            var markersToAdd = _.difference(newProps.markers, this.props.markers)
            var newMarkers = _.map(markersToAdd, this.makeMarker);

            _.each(markersToRemove, function (marker) {
                marker.setMap(null);
            });

            this.setState({markers: _.union(remainingMarkers, newMarkers)})

            if ( newProps.infoWindow !== this.props.infoWindow ) {
                if(this.state.infoWindow) { this.state.infoWindow.close(); }
                this.setState({infoWindow: this.makeInfoWindow(newProps.infoWindow)});
            }
        },

        componentDidUpdate: function () {
            var map = this.state.map;

            map.panTo(this.mapCenterLatLng());
            _(this.state.markers).reject(function (marker) {
                return marker.getMap();
            }).each(function (marker) {
                marker.setMap(map);
            });

            if (this.state.infoWindow) {
                this.state.infoWindow.open(map, this.state.markers.first);
            }
        },

        render: function () {
            var style = {
                width: '100%',
                height: '200px'
            };

            return (
                    <div style={style}></div>
            );
        }
    });

})(window.React, window.app.components);
