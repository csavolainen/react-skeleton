/**
 * @jsx React.DOM
 */
(function(React, components) {
    "use strict";

    components.YouTubeEmbed = React.createClass({
        render: function () {
            var baseUrl = 'http://www.youtube.com/embed/',
            url, queryString;

            url = baseUrl + this.props.videoId
            queryString = this.queryParams();
            if( queryString.length > 1) {
                url += '?' + queryString;
            }

            return <iframe src={url} width='320' height='200' frameBorder='0' />;
        },

        queryParams: function () {
            var queryParams = {};

            if(this.props.startTime) { queryParams.start = this.props.startTime }
            if(this.props.autoPlay) { queryParams.autoplay = this.props.autoPlay ? 1 : 0 }

            return jQuery.param(queryParams);
        }
    });

})(window.React, window.app.components);
