/**
 * @jsx React.DOM
 */
(function(React, pages, components, routes) {
    "use strict";

    pages.Index = React.createClass({
        render: function () {
            var RevealModal = components.foundation.RevealModal;
            var YouTubeEmbed = components.YouTubeEmbed;

            return (
                <div>
                    Hey there. It works.

                    <p>
                      Also there is this <a href='#' data-reveal-id='videoWalkthrough'>popup thing.</a>
                    </p>

                    <RevealModal id='videoWalkthrough'>
                        OMG IT POPS UP AND EVERYTHING
                    </RevealModal>

                    <YouTubeEmbed videoId='5Krz-dyD-UQ' startTime='270' autoPlay={true} />
                </div>
            );
        }
    });
})(window.React, window.app.pages, window.app.components, window.app.routes);
