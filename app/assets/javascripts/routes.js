(function(React, crossroads, pages, routes, $) {
    function createRoute(path, handler, priority) {
        var route = crossroads.addRoute(path, handler, priority);

        return function(params) {
            var path = route.interpolate(params);

            History.pushState(null, null, path);
            crossroads.parse(path);
        };
    }

    routes.index = createRoute('', function() {
        React.renderComponent(
            pages.Index(),
            $('body')[0]
        );
    });
})(window.React, window.crossroads, window.app.pages, window.app.routes, jQuery);
