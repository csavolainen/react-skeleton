require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(:default, Rails.env)

puts File.expand_path('../lib/middle_ware/convert_json.rb', File.dirname(__FILE__))
require File.expand_path('../lib/middle_ware/convert_json.rb', File.dirname(__FILE__))

module ReactSkeleton
  class Application < Rails::Application
    config.time_zone = 'Eastern Time (US & Canada)'

    config.middleware.insert_before(ActionDispatch::ParamsParser, MiddleWare::ConvertJsonResponse)
    config.middleware.insert_before(ActionDispatch::ParamsParser, MiddleWare::ConvertJsonRequest)
  end
end
