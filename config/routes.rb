ReactSkeleton::Application.routes.draw do
  root 'home#index'

  get '/*whatever' => 'home#index', constraints: ->(request) { request.format.html? }

end
