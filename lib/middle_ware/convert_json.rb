require 'mime/types'

module MiddleWare
  def self.json_content_type?(content_type)
    matched_types = MIME::Types[content_type]

    matched_types.present? &&
      matched_types.all? { |mime_type| 'json' == mime_type.sub_type }
  end

  class ConvertJsonResponse
    attr_reader :app

    def initialize(app)
      @app = app
    end

    def call(env)
      status, headers, response = app.call(env)

      if MiddleWare.json_content_type?(headers['Content-Type'])
        [
          status,
          headers,
          response.map { |x| JSONConverter.new(x).camel_case }
        ]
      else
        [status, headers, response]
      end
    end

  end

  class ConvertJsonRequest
    attr_reader :app

    def initialize(app)
      @app = app
    end

    def call(env)
      request = Rack::Request.new(env)
      if MiddleWare.json_content_type?(request.content_type)
        body = env["rack.input"].read
        result = JSONConverter.new(body).snake_case
        env['rack.input'] = StringIO.new(result)
      end

      app.call(env)
    end
  end

  JSONConverter = Struct.new(:body) do
    CAMELIZE = ->(key) { key.camelize(:lower) }
    SNAKE_CASE = ->(key) { key.underscore }

    def camel_case
      transform_with(CAMELIZE)
    end

    def snake_case
      transform_with(SNAKE_CASE)
    end

    private
    def transform_with(transformation)
      json = JSON.load(body)
      new_json = DeepKeyTransformer.new(json, transformation).transform
      JSON.dump(new_json)
    end
  end

  DeepKeyTransformer = Struct.new(:value, :transformation) do
    def transform
      transform_value(value)
    end

    def transform_value(value)
      case value
      when Hash
        transform_hash(value)
      when Array
        transform_array(value)
      else
        value
      end
    end

    def transform_hash(hash)
      hash.each_with_object({}) do |(key, value), new_hash|
        new_value = transform_value(value)
        new_key = transformation.call(key)

        new_hash[new_key] = new_value
      end
    end

    def transform_array(array)
      array.map do |value|
        transform_value(value)
      end
    end
  end
end
