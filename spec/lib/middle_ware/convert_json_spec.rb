require 'spec_helper'

describe MiddleWare::JSONConverter do
  let(:snake_case_json) {
    {
      'some_key' => 'some_value',
      'some_other_key' => {
        'nested_key' => [
          {
            'array_key' => 'value'
          }
        ]
      }
    }.to_json
  }

  let(:camel_case_json) {
    {
      'someKey' => 'some_value',
      'someOtherKey' => {
        'nestedKey' => [
          {
            'arrayKey' => 'value'
          }
        ]
      }
    }.to_json
  }

  it 'converts key to camel case' do
    subject = described_class.new(snake_case_json)

    expect(subject.camel_case).to eq(camel_case_json)
  end

  it 'converts key to snake case' do
    subject = described_class.new(camel_case_json)

    expect(subject.snake_case).to eq(snake_case_json)
  end
end
